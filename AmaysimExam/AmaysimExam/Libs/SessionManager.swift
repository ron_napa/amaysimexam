//
//  SessionManager.swift
//  AmaysimExam
//
//  Created by Ronald Napa on 14/06/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import Foundation

class SessionManager: NSObject {
  
  static let sharedInstance = SessionManager()
  var currentUser:User? = nil
  var currentUserService:Services? = nil
  var currentUserSubscription:Subscriptions? = nil
  var currentUserProduct:Products? = nil
  
  class func readJson() {
    do {
      if let file = Bundle.main.url(forResource: "collection", withExtension: "json") {
        let data = try Data(contentsOf: file)
        let json = try JSONSerialization.jsonObject(with: data, options: [])
        if let object = json as? [String: Any] {
          
          let userInfo = object["data"] as? [String: Any]
          let userAttributes = userInfo?["attributes"] as? [String: Any]
          
          //print(userAttributes!["first-name"]! as! String ?? "")
          let newUser = User.init(payment: userAttributes!["payment-type"] as! String! ?? "",
                                  unbilledCharges: userAttributes!["unbilled-charges"] as! String! ?? "",
                                  nextBillingDate: userAttributes!["next-billing-date"] as! String! ?? "",
                                  title: userAttributes!["title"] as! String! ?? "",
                                  firstName: userAttributes!["first-name"] as! String! ?? "",
                                  lastName: userAttributes!["last-name"] as! String! ?? "",
                                  dateOfBirth: userAttributes!["date-of-birth"] as! String! ?? "",
                                  contactNumber: userAttributes!["contact-number"] as! String! ?? "",
                                  emailAddress: userAttributes!["email-address"] as! String! ?? "",
                                  emailAddressVerified: userAttributes!["email-address-verified"] as! Bool? ?? false,
                                  emailSubscriptionStatus: userAttributes!["email-subscription-status"] as! Bool? ?? false)
          self.sharedInstance.currentUser = newUser

          let userPlan = object["included"] as? [Any]
          let userService = userPlan?[0] as? [String: Any]
          let userServiceAttr = userService?["attributes"] as?[String: Any]
          let userSubscription = userPlan?[1] as? [String: Any]
          let userSubscriptionAttr = userSubscription?["attributes"] as?[String: Any]
          let userProduct = userPlan?[2] as? [String: Any]
          let userProductAttr = userProduct?["attributes"] as?[String: Any]
          
          let newService = Services.init(id: userService!["id"] as! String! ?? "",
                                         msn: userServiceAttr!["msn"] as! String! ?? "",
                                         credit: userServiceAttr!["credit"] as! Int? ?? 0,
                                         creditExpiry: userServiceAttr!["credit-expiry"] as! String! ?? "",
                                         dataUsageThreshold: userServiceAttr!["data-usage-threshold"] as! Bool? ?? false)
          self.sharedInstance.currentUserService = newService
          
          let newSubscription = Subscriptions.init(id: userSubscription!["id"] as! String! ?? "",
                                                   dataBalance: userSubscriptionAttr!["included-data-balance"] as! Int? ?? 0,
                                                   creditBalance: userSubscriptionAttr!["included-credit-balance"] as! Int? ?? 0,
                                                   rollOverCreditBalance: userSubscriptionAttr!["included-rollover-credit-balance"] as! Int? ?? 0,
                                                   rollOverDataBalance: userSubscriptionAttr!["included-rollover-data-balance"] as! Int? ?? 0,
                                                   internationalTalkBalance: userSubscriptionAttr!["included-international-talk-balance"] as! Int? ?? 0,
                                                   expiryDate: userSubscriptionAttr!["expiry-date"] as! String! ?? "",
                                                   autoRenewal: userSubscriptionAttr!["auto-renewal"] as! Bool? ?? false,
                                                   primarySubscription: userSubscriptionAttr!["primary-subscription"] as! Bool? ?? false)
          self.sharedInstance.currentUserSubscription = newSubscription
          
          let newProduct = Products.init(id: userProduct!["id"] as! String! ?? "",
                                         name: userProductAttr!["name"] as! String! ?? "",
                                         data: userProductAttr!["included-data"] as! String! ?? "",
                                         credit: userProductAttr!["included-credit"] as! String! ?? "",
                                         internationalTalk: userProductAttr!["included-international-talk"] as! String! ?? "",
                                         unliText: userProductAttr!["unlimited-text"] as! Bool? ?? false,
                                         unliTalk: userProductAttr!["unlimited-talk"] as! Bool? ?? false,
                                         unliInternationalText: userProductAttr!["unlimited-international-text"] as! Bool? ?? false,
                                         unliInternationalTalk: userProductAttr!["unlimited-international-talk"] as! Bool? ?? false,
                                         price: userProductAttr!["price"] as! Int? ?? 0)
          self.sharedInstance.currentUserProduct = newProduct
          
        } else if let object = json as? [Any] {
          
          print(object)
        } else {
          print("JSON is invalid")
        }
      } else {
        print("no file")
      }
    } catch {
      print(error.localizedDescription)
    }
  }
  
}
