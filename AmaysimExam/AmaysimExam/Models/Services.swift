//
//  Services.swift
//  AmaysimExam
//
//  Created by Ronald Napa on 14/06/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import Foundation

class Services : NSObject{
  
  var id = ""
  var msn = ""
  var credit = 0
  var creditExpiry = ""
  var dataUsageThreshold = false
  
  convenience init(id:String,
                   msn:String,
                   credit:Int,
                   creditExpiry:String,
                   dataUsageThreshold:Bool) {
    self.init()
    
    self.id = id
    self.msn = msn
    self.credit = credit
    self.creditExpiry = creditExpiry
    self.dataUsageThreshold = dataUsageThreshold
    
  }
  
}
