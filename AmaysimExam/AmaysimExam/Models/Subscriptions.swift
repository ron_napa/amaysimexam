//
//  Subscriptions.swift
//  AmaysimExam
//
//  Created by Ronald Napa on 14/06/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import Foundation

class Subscriptions : NSObject{
  
  var id = ""
  var dataBalance = 0
  var creditBalance = 0
  var rollOverCreditBalance = 0
  var rollOverDataBalance = 0
  var internationalTalkBalance = 0
  var expiryDate = ""
  var autoRenewal = false
  var primarySubscription = false
  
  convenience init(id:String,
                   dataBalance:Int,
                   creditBalance:Int,
                   rollOverCreditBalance:Int,
                   rollOverDataBalance:Int,
                   internationalTalkBalance:Int,
                   expiryDate:String,
                   autoRenewal:Bool,
                   primarySubscription:Bool) {
    self.init()
    
    self.id = id
    self.dataBalance = dataBalance
    self.creditBalance = creditBalance
    self.rollOverCreditBalance = rollOverCreditBalance
    self.rollOverDataBalance = rollOverDataBalance
    self.internationalTalkBalance = internationalTalkBalance
    self.expiryDate = expiryDate
    self.autoRenewal = autoRenewal
    self.primarySubscription = primarySubscription
    
  }
  
}
