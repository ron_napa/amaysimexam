//
//  User.swift
//  AmaysimExam
//
//  Created by Ronald Napa on 14/06/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import Foundation
import UIKit

class User : NSObject{

  var paymentType:String! = ""
  var unbilledCharges:String! = ""
  var nextBillingDate:String! = ""
  var title:String! = ""
  var firstName:String! = ""
  var lastName:String! = ""
  var dateOfBirth:String! = ""
  var contactNumber:String! = ""
  var emailAddress:String! = ""
  var emailAddressVerified:Bool! = false
  var emailSubscriptionStatus:Bool! = false
  
  convenience init(payment:String!,
                   unbilledCharges:String!,
                   nextBillingDate:String!,
                   title:String!,
                   firstName:String!,
                   lastName:String!,
                   dateOfBirth:String!,
                   contactNumber:String!,
                   emailAddress:String!,
                   emailAddressVerified:Bool!,
                   emailSubscriptionStatus:Bool!) {
    self.init()
    self.paymentType = payment!
    self.unbilledCharges = unbilledCharges!
    self.nextBillingDate = nextBillingDate!
    self.title = title!
    self.firstName = firstName!
    self.lastName = lastName!
    self.dateOfBirth = dateOfBirth!
    self.contactNumber = contactNumber!
    self.emailAddress = emailAddress!
    self.emailAddressVerified = emailAddressVerified!
    self.emailSubscriptionStatus = emailSubscriptionStatus!
    
    print(self.firstName!)
  }
  
}
