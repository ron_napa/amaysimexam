//
//  Products.swift
//  AmaysimExam
//
//  Created by Ronald Napa on 14/06/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import Foundation

class Products : NSObject{
  
  var id = ""
  var name = ""
  var data = ""
  var credit = ""
  var internationalTalk = ""
  var unliText = false
  var unliTalk = false
  var unliInternationalText = false
  var unliInternationalTalk = false
  var price = 0
  
  convenience init(id:String,
                   name:String,
                   data:String,
                   credit:String,
                   internationalTalk:String,
                   unliText:Bool,
                   unliTalk:Bool,
                   unliInternationalText:Bool,
                   unliInternationalTalk:Bool,
                   price:Int) {
    self.init()
    
    self.id = id
    self.name = name
    self.data = data
    self.credit = credit
    self.internationalTalk = internationalTalk
    self.unliText = unliText
    self.unliTalk = unliTalk
    self.unliInternationalTalk = unliInternationalTalk
    self.unliInternationalText = unliInternationalText
    self.price = price
    
  }
  
}
