//
//  ViewController.swift
//  AmaysimExam
//
//  Created by Ronald Napa on 14/06/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

  @IBOutlet weak var emailLabel: UITextField!
  @IBOutlet weak var passwordLabel: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func login(_ sender: Any) {
    if(emailLabel.text?.caseInsensitiveCompare((SessionManager.sharedInstance.currentUser?.emailAddress)!) == ComparisonResult.orderedSame){
      performSegue(withIdentifier: "showProfile", sender: nil)
    }else{
      let alert = UIAlertController(title: "Error", message: "Wrong Email", preferredStyle: UIAlertControllerStyle.alert)
      alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
      self.present(alert, animated: true, completion: nil)
    }
  }

}

