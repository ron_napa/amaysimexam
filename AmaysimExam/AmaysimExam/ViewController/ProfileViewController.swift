//
//  ProfileViewController.swift
//  AmaysimExam
//
//  Created by Ronald Napa on 14/06/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

  @IBOutlet weak var userGreetingLabel: UILabel!
  @IBOutlet weak var userBalanceLabel: UILabel!
  @IBOutlet weak var userMSNLabel: UILabel!
  @IBOutlet weak var userExpiryLabel: UILabel!
  @IBOutlet weak var userSubsNameLabel: UILabel!
  @IBOutlet weak var userSubsDataLabel: UILabel!
  @IBOutlet weak var userSubsExpiryLabel: UILabel!
  
  var user:User!
  var service:Services!
  var subscription:Subscriptions!
  var product:Products!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        user = SessionManager.sharedInstance.currentUser
        service = SessionManager.sharedInstance.currentUserService
        subscription = SessionManager.sharedInstance.currentUserSubscription
        product = SessionManager.sharedInstance.currentUserProduct
      
        loadAllText()
        // Do any additional setup after loading the view.
    }

    func loadAllText(){
      
      userGreetingLabel.text = "\(user.title!) \(user.firstName!) \(user.lastName!)"
      userBalanceLabel.text = "\(service.credit)"
      userMSNLabel.text = service.msn
      userExpiryLabel.text = service.creditExpiry
      userSubsNameLabel.text = product.name
      userSubsDataLabel.text = "\(subscription.dataBalance)"
      userSubsExpiryLabel.text = subscription.expiryDate
    
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
